<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/registerUser', 'UsuarioController@create')->name('home');
Route::get('/getProfiles', 'HomeController@home')->name('home')->middleware('auth');

Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::post('/register', 'Auth\RegisterController@register');

Route::post('/viewCreateTicket', 'TicketController@viewCreate');
Route::post('/createTicket', 'TicketController@create');
Route::post('/viewEditTicket', 'TicketController@viewEdit');
Route::post('/editTicket', 'TicketController@edit');
Route::post('/viewDeleteTicket', 'TicketController@viewDelete');
Route::post('/deleteTicket', 'TicketController@delete');

Route::post('/UpdateStatusTicket', 'TicketController@UpdateStatus');
