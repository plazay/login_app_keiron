@extends('layouts.app')

@section('content')
<div class="main" style="background-image: url('images/peli.jpg')">
    <div class="cover orange" data-color="orange"></div>
    <div class="container" style="padding-top:300px">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="z-index:300000;">
                    <div class="card-header">Editar ticket</div>

                    <div class="card-body">
                        <form method="POST" action="{{ url('editTicket') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Descripcion del pedido</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" value="{{$pedido}}" name="pedido" required autofocus>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Asignar a</label>

                                <div class="col-md-6">
                                    <select class="form-control" name ="usuario" id="usuario">
                                        <option value="" disabled selected></option>
                                        @foreach($usuarios as $usuario)
                                            <option value="{{$usuario->id}}">{{$usuario->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <input type="hidden" name="id" value="{{$idPedido}}">
                                    <button type="submit" class="btn btn-primary">
                                        Guardar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
