@extends('layouts.app')

@section('content')
<div class="main" style="background-image: url('images/peli.jpg')">
    <div class="cover orange" data-color="orange"></div>
    <div class="container" style="padding-top:300px">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="z-index:300000">
                    <div class="card-header">Eliminar Pedido</div>

                    <div class="card-body">
                        <form method="POST" action="{{ url('deleteTicket') }}">
                            @csrf

                            <div class="form-group">
                                <h3  class="col-md-12 text-center">Esta por eliminar el siguiente pedido ¿seguro que desea borrarlo?</h3>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Descripcion del pedido</label>

                                <div class="col-md-6">
                                    <h4 class="form-control">{{$pedido}}</h4>

                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <input type="hidden" name="id" value="{{$idPedido}}">
                                    <button type="submit" class="btn btn-danger">
                                        Eliminar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
