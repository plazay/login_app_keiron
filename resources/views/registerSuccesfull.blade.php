@extends('layouts.app')

@section('content')
<div class="main" style="background-image: url('images/peli.jpg')">
    <div class="cover orange" data-color="orange"></div>
    <div class="container" style="padding-top:300px">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Mensaje</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        El registro se ha realizado satisfactoriamente!
                    </div>
                    <div class="card-body">
                        <p>Lo invitamos a ingresar al sistema en la parte superior derecha y presionar el boton </p><p class="text-info">'login'</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
