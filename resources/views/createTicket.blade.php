@extends('layouts.app')

@section('content')
<div class="main" style="background-image: url('images/peli.jpg')">
    <div class="cover orange" data-color="orange"></div>
    <div class="container" style="padding-top:300px">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="z-index:300000;">
                    <div class="card-header">Nuevo ticket</div>

                    <div class="card-body">
                        <form method="POST" action="{{ url('createTicket') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Descripcion del pedido</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="pedido" required autocomplete="name" autofocus>

                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Crear
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
