<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        
        <title>AppKeiron </title>
        
        <link href="css/bootstrap.css" rel="stylesheet" />
        <link href="css/coming-sssoon.css" rel="stylesheet" />    
        
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
    
    </head>
    <body>
        <nav class="navbar navbar-transparent navbar-fixed-top" role="navigation">  
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'AppKeirón') }}
                    </a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        @if (Route::has('login'))
                            @auth
                                <li>
                                    <a href="{{ url('/home') }}">Home</a>
                                </li>
                            @else
                                <li>
                                    <a href="{{ route('login') }}">Login</a>
                                </li>
                                @if (Route::has('register'))
                                    <li>
                                        <a href="{{ route('register') }}">Registrarse</a>
                                    </li>
                                @endif
                            @endauth
                        @endif
                    </ul>
                
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container -->
        </nav>
        <div class="main" style="background-image: url('images/peli.jpg')">
            <div class="cover orange" data-color="orange"></div>

            <div class="container">
                <div class="content" style="padding:200px">                   
                    <div class="subscribe">
                        <h1 class="logo cursive">
                            AppKeirón
                        </h1>
                        <h4 class="motto">Donde podras gestionar todos tus pédidos</h4>
                        <!-- <div class="row">
                            <div class="col-md-4 col-md-offset-4 col-sm6-6 col-sm-offset-3 ">
                                <form class="form-inline" id="search" method="GET" action="{{ url('getDataMovie') }}">
                                <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                    <input class="form-control transparent" placeholder="Ej: Batman">
                                </div>
                                <button type="submit" class="btn btn-danger btn-fill">Buscar</button>
                                </form>

                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
</html>