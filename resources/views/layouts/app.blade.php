<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <title>AppKeiron </title>

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Poppins:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
        <!-- <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'> -->
        <link href=" css/app.css" rel="stylesheet">
        <link href="css/coming-sssoon.css" rel="stylesheet" />
        <link href="css/bootstrap.css" rel="stylesheet" />

    </head>
<body>
    <!-- <div id="app"> -->
        <nav class="navbar navbar-transparent navbar-fixed-top" role="navigation"> 
            <div class="container">


                <!-- <div class="collapse navbar-collapse" id="navbarSupportedContent"> -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="navbar-header collapse navbar-collapse">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            {{ config('app.name', 'AppKeirón') }}
                        </a>
                    </div>
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right collapse navbar-collapse">
                        <!-- Authentication Links -->
                        @guest
                            <li>
                                <a href="{{ url('/login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li>
                                    <a href="{{ url('/register') }}">{{ __('Registrarse') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @if(Auth::user()->id_tipouser == 1)
                                        <a class="dropdown-item">
                                            Administrador
                                        </a><hr>
                                    @endif
                                    @if(Auth::user()->id_tipouser == 2)
                                        <a class="dropdown-item">
                                            Usuario
                                        </a><hr>
                                    @endif
                                    <a class="dropdown-item" href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar session') }}
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
        <div class="footer">
            <div class="container">
                    Creado por <a href="#">Yoneiker Plaza <i class="fa fa-hand-peace-o"></i>
            </div>
        </div>
    <!-- </div> -->
</body>
<!-- <script src="js/app.js" defer></script> -->
<script src="js/coming-sssoon.js" type="text/javascript"></script>
<script src="js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
</html>
