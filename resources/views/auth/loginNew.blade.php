@extends('layouts.app')

@section('content')

  <div class="content">
    <div class="d-flex justify-content-center">
        <div class="login-form">
          <div class="card">
              <div class="card-header-mst" data-background-color="blue">
                <h4 class="title text-white no-margin"><i class="material-icons">fingerprint</i></i> Login</h4>
                <p class="category">Ingrese su email y contraseña</p>
              </div>
              <form id="Login" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="modal-body">
                  <div class="col-md-6">
                    <div class="d-flex justify-content-between">
                        <i class="material-icons" style="padding-top: 12px;">email</i>
                      <div class="group">
                        <input id="email" type="email"  class="{{ $errors->has('email') ? ' is-invalid' : '' }} long" name="email" value="{{ old('email') }}" style="width:275px" required>
                        <span class="highlight"></span>
                        <span class="bar" style="width:275px"></span>
                        <label class="label-mst">Email</label>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
                    <div class="d-flex justify-content-between">
                      <i class="material-icons" style="padding-top: 12px;">lock_outline</i>
                      <div class="group">

                        <input id="password" type="password"  class="{{ $errors->has('password') ? ' is-invalid' : '' }} long" name="password" style="width:275px" required>
                        <span class="highlight"></span>                        
                        <span class="bar" style="width:275px"></span>
                        <label class="label-mst">Contraseña</label>
                        @if ($errors->has('password'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <div class="col-sm-12">

                      <button type="submit" class="btn btn-primary">
                          {{ __('Entrar') }}
                      </button>
                    </div>
                  </div>
                </div>              
              </form>
            </div>
          </div>
        </div>
    </div>
  </div>
@endsection
