@extends('layouts.app')

@section('content')
<div class="main" style="background-image: url('images/peli.jpg')">
<div class="cover orange" data-color="orange"></div>
    <div class="container" style="padding-top:300px">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="z-index:300000;">
                    <div class="card-header">Pedidos</div>
                    <div class="animated fadeIn table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Pedido</th>
                                    <th>Estado</th>
                                    <th>Fecha de creación</th>
                                    <th class="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(Auth::user()->id_tipouser == 1)
                                    @foreach($tickets as $ticket)
                                        <tr>
                                            <td>{{$ticket->id}}</td>
                                            <td>{{$ticket->ticket_pedido}}</td> 
                                            @if($ticket->status == 'Pendiente')
                                                <td><span class="text-danger">{{$ticket->status}}</span></td>
                                            @else
                                                <td><span class="text-success">{{$ticket->status}}</span></td>
                                            @endif
                                            <td>{{date('d-m-Y', strtotime($ticket->created_at))}}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <form method="POST" action="{{ url('/viewCreateTicket') }}">
                                                        @csrf
                                                        <button type="submit" data-toggle="tooltip" title="Crear pedido" class="btn btn-success btn-xs"><i class="fa fa-plus"></i></button>
                                                    </form>
                                                    <form method="POST" action="{{ url('/viewEditTicket') }}">
                                                        @csrf
                                                        <input type="hidden" name="dataTicket" value="{{$ticket}}">
                                                        <button type="submit" data-toggle="tooltip" title="Editar y asignar pedido" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></button>
                                                    </form>
                                                    <form method="POST" action="{{ url('/viewDeleteTicket') }}">
                                                        @csrf
                                                        <input type="hidden" name="dataTicket" value="{{$ticket}}">
                                                        <button type="submit" data-toggle="tooltip" title="Eliminar pedido" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    @foreach($tickets as $ticket)
                                        <tr>
                                            <td>{{$ticket->id}}</td>
                                            <td>{{$ticket->ticket_pedido}}</td>
                                            @if($ticket->status == 'Pendiente')
                                                <td><span class="text-danger">{{$ticket->status}}</span></td>
                                            @else
                                                <td><span class="text-success">{{$ticket->status}}</span></td>
                                            @endif
                                            <td>{{date('d-m-Y', strtotime($ticket->created_at))}}</td>
                                            @if($ticket->status == 'Pendiente')
                                                <td>
                                                    <form method="POST" action="{{ url('/UpdateStatusTicket') }}">
                                                        @csrf
                                                        <input type="hidden" name="dataTicket" value="{{$ticket}}">
                                                        <button type="submit" class="btn btn-info btn-xs">Pedir</button>
                                                    </form>
                                                </td>
                                            @else
                                                <td>
                                                    <i class="text-success text-center fa fa-check"></i>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
