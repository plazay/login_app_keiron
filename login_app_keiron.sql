﻿# Host: 127.0.0.1  (Version 5.7.19)
# Date: 2020-03-09 17:51:28
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "migrations"
#

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "migrations"
#

INSERT INTO `migrations` VALUES (1,'2020_03_08_005704_create_usuarios_table',1),(2,'2020_03_08_005752_create_tickets_table',1),(3,'2020_03_08_005810_create_tipo_usuarios_table',1),(4,'2014_10_12_000000_create_users_table',2),(5,'2020_03_09_151011_alter_users_table',3),(6,'2020_03_09_154437_alter_ticket_table',4),(7,'2020_03_09_193209_alter_ticket_table_2',5);

#
# Structure for table "tickets"
#

CREATE TABLE `tickets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `ticket_pedido` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'Pendiente',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tickets_ticket_pedido_unique` (`ticket_pedido`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "tickets"
#

INSERT INTO `tickets` VALUES (1,2,'Revisar equipo de oficina','Asignado','2020-03-08 23:33:12','2020-03-09 20:41:01',NULL),(2,1,'Enviar notebook a usuario.','Asignado','2020-03-09 16:43:33','2020-03-09 20:05:07',NULL);

#
# Structure for table "tipo_usuarios"
#

CREATE TABLE `tipo_usuarios` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "tipo_usuarios"
#

INSERT INTO `tipo_usuarios` VALUES (1,'Administrador','2020-01-08 19:49:00','2019-01-08 19:49:00'),(2,'Usuario','2020-01-08 19:55:20','2020-01-08 19:55:20');

#
# Structure for table "users"
#

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_tipouser` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "users"
#

INSERT INTO `users` VALUES (1,'1','Yoneiker Plaza','carterplaza4@gmail.com',NULL,'$2y$10$NYeX.7lEPBCpLc/gPlIFaObQJI843xpCP.CZVsWDo2DbI5xSX36ju',NULL,'2020-03-08 23:33:12','2020-03-08 23:33:12'),(2,'2','Gisela Stein','gisteinm@gmail.com',NULL,'$2y$10$4xzPfTeHJVl42c1H1XyhauEyC72u0XVr1nLc5AFkciL4URCd/cbzq',NULL,'2020-03-09 10:18:26','2020-03-09 10:18:26'),(3,'','Felipe','yoneiyor-f21@hotmail.com',NULL,'$2y$10$nQU4gueYoG.9rZ9AElzitu7SCsax4D4RdXoCqc5I99I/KECLOi8VK',NULL,'2020-03-09 16:49:10','2020-03-09 16:49:10');
