<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    protected function viewCreate()
    {
      
        return view('/createTicket');

    }
    public function create(Request $request)
    {
        $ticket = new Ticket;
        $ticket->ticket_pedido = $request->pedido;
        $ticket->id_user = Auth::user()->id;

        $ticket->save();


        return redirect()->intended('/home')->with(['tipo'=>'success','pass'=>'error','mensaje'=>'El cliente ha sido creado satisfactoriamente']);

    }
    protected function viewEdit(Request $request)
    {
        $dataTicket = json_decode($request->dataTicket);
        $idPedido = $dataTicket->id;
        $pedido = $dataTicket->ticket_pedido;
        $usuarios=User::all();

        return view('/editTicket', compact('idPedido', 'pedido', 'usuarios'));

    }
    public function edit(Request $request)
    {
        // return $request->usuario;
        if(!isset($request->usuario)){
            $request->usuario = Auth::user()->id;
        }
        $ticket = Ticket::find($request->id);
        $ticket->id_user = $request->usuario;
        $ticket->ticket_pedido = $request->pedido;
        $ticket->status = 'Asignado';                        
        $ticket->save();

        return redirect()->intended('/home')->with(['tipo'=>'success','pass'=>'error','mensaje'=>'Cliente actualizado satisfactoriamente']);
    }
    
    public function UpdateStatus(Request $request)
    {
        // return $request;
        $dataTicket = json_decode($request->dataTicket);
        $idPedido = $dataTicket->id;
        $ticket = Ticket::find($idPedido);

        $ticket->status = 'Asignado';
        $ticket->save();
        
        return redirect()->intended('/home');
    }
    protected function viewDelete(Request $request)
    {
        $dataTicket = json_decode($request->dataTicket);
        $idPedido = $dataTicket->id;
        $pedido = $dataTicket->ticket_pedido;

        return view('/deleteTicket', compact('idPedido', 'pedido'));

    }
    public function delete(Request $request)
    {
        $ticket = Ticket::destroy($request->id);

        return redirect()->intended('/home')->with(['tipo'=>'success','pass'=>'error','mensaje'=>'El cliente ha sido eliminado']);
    }

}
