<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class UsuarioController extends Controller
{
    protected $redirectTo = '/home';
    protected function create(Request $data)
    {
        if($data['perfil']=='Administrador'){
            $data['perfil'] = 1;
        }
        if($data['perfil']=='Usuario'){
            $data['perfil'] = 2;
        }
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'id_tipouser' => $data['perfil'],
        ]);
        return view('registerSuccesfull');
    }
}
