<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\User;
use App\TipoUsuario;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tickets=Ticket::all();
        $usuarios=User::all();
        return view('home', compact('tickets'));
    }
    public function home()
    {
        $profile = TipoUsuario::all();
        return view('register', compact ('profile'));
    }
}
